const Joi = require("joi");

const personValidation = data => {
  const schema = {
    name: Joi.string().required(),
    shortname: Joi.string().required(),
    known: Joi.string().required(),
    bio: Joi.string().required()
  };

  return Joi.validate(data, schema);
};

module.exports.personValidation = personValidation;
