emerging-it testing project

# Data List panel

please enter serach filed value to filter data.

# Profile

please click any one of the row from the table in List url to get details about the person on profile url
or
you can enter url in profile/user id to get all the information about the person

# Add Person

This form is for adding new person information. For this initial stage no image upload option hasn't been implemented. So the new person will not contain any images. But adding new person data will be present in both list and profile page.

# Api Test

This for testing api using express in backend. This api will return all the person data in json formate.

# Tools / Frameworks

--Backend => Nodejs, Expressjs
--Database => MongoDB
--Frontend => React js

Thank you
